import 'package:flutter/material.dart';
import 'package:base_flutter_app/pages/user_list_page.dart';
import 'package:base_flutter_app/viewmodels/user_list_view_model.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Users",
        home:
        ChangeNotifierProvider(
          create: (context) => UserListViewModel(),
          child: UserListPage(),
        )
    );
  }
}
