
import 'package:base_flutter_app/model/user.dart';

class UserViewModel {

  final User user;

  UserViewModel({this.user});

  String get id {
    return this.user.id;
  }

  String get name {
    return this.user.name;
  }

  String get avatar {
    return this.user.avatar;
  }

  String get email {
    return this.user.email;
  }

}