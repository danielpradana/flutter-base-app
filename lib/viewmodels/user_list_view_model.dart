

import 'package:flutter/material.dart';
import 'package:base_flutter_app/services/webservice.dart';
import 'package:base_flutter_app/viewmodels/user_view_model.dart';

class UserListViewModel extends ChangeNotifier {

  List<UserViewModel> users = List<UserViewModel>();

  Future<void> fetchUsers() async {
    final results =  await Webservice().fetchUser();
    this.users = results.map((item) => UserViewModel(user: item)).toList();
    print(this.users);
    notifyListeners(); 
  }

}