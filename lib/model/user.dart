
class User {
  String id;
  String name;
  String avatar;
  String email;

  User({this.id, this.name, this.avatar, this.email});

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    name: json["name"],
    avatar: json["avatar"],
    email: json["email"]
  );
}