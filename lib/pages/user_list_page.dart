
import 'package:flutter/material.dart';
import 'package:base_flutter_app/viewmodels/user_list_view_model.dart';
import 'package:base_flutter_app/widgets/user_list.dart';
import 'package:provider/provider.dart';

class UserListPage extends StatefulWidget {
  @override 
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {

  @override
  void initState() {
    super.initState();
    // you can uncomment this to get all batman movies when the page is loaded
    //Provider.of<MovieListViewModel>(context, listen: false).fetchMovies("batman");
  }

  @override
  Widget build(BuildContext context) {

    final vm = Provider.of<UserListViewModel>(context);
    vm.fetchUsers();
    print("data: "+vm.users.toString());

    return Scaffold(
      appBar: AppBar(
        title: Text("Users")
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: UserList(users: vm.users))
    );
  }
}