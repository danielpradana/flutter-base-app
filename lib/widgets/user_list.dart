
import 'package:flutter/material.dart';
import 'package:base_flutter_app/viewmodels/user_view_model.dart';

class UserList extends StatelessWidget {

  final List<UserViewModel> users;

  UserList({this.users});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.users.length,
      itemBuilder: (context, index) {
        
        final item = this.users[index];

        return Card(child: ListTile(
          contentPadding: EdgeInsets.all(10),
          leading: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(item.avatar)
              ),
              borderRadius: BorderRadius.circular(6)
            ),
            width: 50, 
            height: 100,
            ),
          title: Text(item.name),
          subtitle: Text(item.email),
        ));
      },
    );
  }
}