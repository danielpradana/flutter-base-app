
import 'dart:convert';

import 'package:base_flutter_app/model/user.dart';
import 'package:http/http.dart' as http;

class Webservice {

  Future<List<User>> fetchUser() async {

    final url = "https://5e510330f2c0d300147c034c.mockapi.io/users";
    final response = await http.get(url);
    if(response.statusCode == 200) {
       final body = jsonDecode(response.body) as List;
       return body.map((user) => User.fromJson(user)).toList();

    } else {
      throw Exception("Unable to perform request!");
    }
  }
}